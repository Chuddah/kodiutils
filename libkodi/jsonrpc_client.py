__all__ = ('Client', 'call_server_method')

import requests, json

def call_server_method(kodi_server, id, func, **params):
    headers = {'Content-Type': 'application/json'}
    data = {'id': id, 'jsonrpc': '2.0', 'method': func, 'params': params}
    resp = requests.post(kodi_server, headers=headers, data=json.dumps(data))
    json_resp = resp.json()

    if 'error' in json_resp:
        raise ValueError(json_resp['error']['message'])

    return json_resp['result']

ConnectionError = requests.exceptions.ConnectionError

class Client(object):

    def __init__(self, endpoint):
        self._endpoint = endpoint
        self._id = 0

    def call_server_method(self, method_name, **kargs):
        self._id += 1
        return call_server_method(self._endpoint, self._id, method_name, **kargs)

    def __getattr__(self, name):
        return Namespace(self, [name])

    def __str__(self):
        return 'Connection to {}'.format(self._endpoint)

class Namespace(object):
    def __init__(self, endpoint, namespace):
        self._endpoint = endpoint
        self._namespace = namespace

    def __getattr__(self, name):
        return Namespace(self._endpoint, self._namespace + [name])

    def __call__(self, **kargs):
        return self._endpoint.call_server_method('.'.join(self._namespace), **kargs)


