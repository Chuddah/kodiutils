import jsonrpc_client

ConnectionError = jsonrpc_client.ConnectionError

def endpoint(host, port=8080):
    url = 'http://{host}:{port}/jsonrpc'.format(**locals())
    return jsonrpc_client.Client(url)

class Players(object):
    Video = 1

