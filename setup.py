#!/usr/bin/env python

from setuptools import setup

setup(name='libkodi',
      description='Kodi Utils',
      version='1.0.00',
      author='Damien Ruscoe',
      maintainer='Damien Ruscoe',
      maintainer_email='damienruscoe@gmail.com',
      url='http://www.damienruscoe.co.uk',
      packages=[
          'libkodi',
          'scripts',
      ],
      install_requires=[
          'zeroconf',
      ],
      platforms=['linux', 'windows', 'mac'],
      classifiers=[
          'Development Status :: 4 - Beta',
          'Environment :: Console',
          'Environment :: Testing',
          'Intended Audience :: Kodi Users',
          'Operating System :: MacOS :: MacOS X',
          'Operating System :: Microsoft :: Windows',
          'Operating System :: POSIX',
          'Programming Language :: Python',
          'Topic :: Communications :: Email',
          'Topic :: Software Development :: Testing',
          ],
      entry_points = {
          'console_scripts': [
                'kodi = scripts.kodi:main',
                'kodi_transfer = scripts.transfer:main',
                'kodi_play_youtube = scripts.play_youtube:main',
                'kodi_ping = scripts.ping:main',
                'kodi_playlist = scripts.playlist:main',
                'kodi_find = scripts.find:main',
                'kodi_update_library = scripts.update_library:main',
                'kodi_permissions = scripts.permissions:main',
          ],
      },
      package_data={
          #'sn.toolchain': ['deploy_scripts/*'],
      },
      include_package_data=True,
)

