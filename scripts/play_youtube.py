from libkodi import kodi

""" KODI Play Youtube docs """

def add_arguments(parser):
    parser.add_argument('--server', help='Server to play video',
                                    default='localhost')
    parser.add_argument('--queue', help='Append the video to the playlist',
                                    default=False, action='store_true')
    parser.add_argument('urls', help='Url to play', 
                                    metavar='youtube_url', nargs='+')

def build_parser():
    import argparse
    parser = argparse.ArgumentParser(description=__doc__)
    add_arguments(parser)
    return parser

def youtube_url_to_plugin_urn(youtube_url):
    from urlparse import urlparse, parse_qs

    parsed = urlparse(youtube_url)
    query = parse_qs(parsed.query)

    return "plugin://plugin.video.youtube/?action=play_video&videoid={query[v][0]}".format(query=query)

def queue_youtube_stream(dest, youtube_url):
    file = youtube_url_to_plugin_urn(youtube_url)
    dest.Playlist.Add(playlistid=1, item={"file": file})

def play_youtube_stream(dest, youtube_url):
    file = youtube_url_to_plugin_urn(youtube_url)
    dest.Player.Open(item={"file": file})

def main(argv=None):
    args = build_parser().parse_args(argv)
    dest = kodi.endpoint(args.server)
    try:
        if args.queue:
            for url in args.urls:
                queue_youtube_stream(dest, url)
        else:
            play_youtube_stream(dest, args.urls[0])
            for url in args.urls[1:]:
                queue_youtube_stream(dest, url)


    except kodi.ConnectionError as e:
        print e.message.args[1]
    except Exception as e:
        print e

if __name__ == '__main__':
    main()
