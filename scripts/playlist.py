from libkodi import kodi

""" KODI Play Youtube docs """

def add_arguments(parser):
    parser.add_argument('--server', help='Server to play video',
                                    default='localhost')
    parser.add_argument('--clear',  help='Clear the playlist',
                                    default=False, action='store_true')

def build_parser():
    import argparse
    parser = argparse.ArgumentParser(description=__doc__)
    add_arguments(parser)
    return parser

def main(argv=None):
    args = build_parser().parse_args(argv)
    dest = kodi.endpoint(args.server)
    try:
        if args.clear:
            dest.Playlist.Clear(playlistid=1)
        else:
            items = dest.Playlist.GetItems(playlistid=1, properties=['title'])
            try:
                items = items['items']
            except KeyError:
                print 'Empty Playlist'
            else:
                for item in items:
                    print '{item[type]}: {item[label]}'.format(item=item)
    except kodi.ConnectionError as e:
        print 'Connection Error'
    except Exception as e:
        print e

if __name__ == '__main__':
    main()
