from libkodi import kodi

""" KODI Transfer docs """

def add_arguments(parser):
    parser.add_argument('servers', help='Server to transfer data between',
                        nargs=2)

def build_parser():
    import argparse
    parser = argparse.ArgumentParser(description=__doc__)
    add_arguments(parser)
    return parser

def transfer(source_kodi, destination_kodi):
    active = source_kodi.Player.GetActivePlayers()
    if not active:
        raise ValueError('Nothing playing')

    for player in active:
        player_id = player['playerid']

        current_item = source_kodi.Player.GetItem(playerid=player_id, properties=['file'])
        current_time = source_kodi.Player.GetProperties(playerid=player_id, properties=['time'])

        destination_kodi.Player.Open(item={'file': current_item['item']['file']})
        destination_kodi.Player.Seek(playerid=player_id, value=current_time)

def main(argv=None):
    args = build_parser().parse_args(argv)

    source = kodi.endpoint(args.servers[0])
    dest =   kodi.endpoint(args.servers[1])

    try:
        transfer(source, dest)
    except kodi.ConnectionError as e:
        print "Connection Error"
        exit(1)
    except Exception as e:
        print e
        exit(1)

    print result

if __name__ == '__main__':
    main()
