import time
from six.moves import input
from zeroconf import ServiceBrowser, Zeroconf

from libkodi import kodi

class ZeroConfListener(object):

    def remove_service(self, zeroconf, type, name):
        print("Service %s removed" % (name,))

    def add_service(self, zeroconf, type, name):
        info = zeroconf.get_service_info(type, name)
        name = name.split('.')[0]

        ip = '.'.join(str(ord(x)) for x in info.address)

        kodi_server = kodi.endpoint(ip, info.port)
        #result = kodi_server.Application.GetProperties(properties=['name'])
        #name = result['name']

        print 'Added', name, ip, info.port

class Interactive(object):
    def wait(self):
        input("Press enter to exit...\n\n")

class Timer(object):
    def __init__(self, seconds):
        self._seconds = seconds

    def wait(self):
        time.sleep(self._seconds)

def find(service_name, end_condition):
    zeroconf = Zeroconf()
    listener = ZeroConfListener()
    browser = ServiceBrowser(zeroconf, service_name, listener)
    try:
        end_condition.wait()
    finally:
        zeroconf.close()


def add_arguments(parser):
    parser.add_argument('--timeout', help='Maximum search time for searching',
                        metavar='seconds', type=int)

def build_parser():
    import argparse
    parser = argparse.ArgumentParser(description=__doc__)
    add_arguments(parser)
    return parser

def main(argv=None):
    args = build_parser().parse_args(argv)
    if args.timeout:
        end_condition = Timer(seconds=args.timeout)
    else:
        end_condition = Interactive()
    find("_xbmc-jsonrpc._tcp.local.", end_condition=end_condition)
    #find("_xbmc-jsonrpc-h._tcp.local.", end_condition=end_condition)

def zeroconf_find_all():
    from zeroconf import ZeroconfServiceTypes
    print('\n'.join(ZeroconfServiceTypes.find()))

if __name__ == '__main__':
    #zeroconf_find_all()
    main()
