import sys

""" KODI Utils """

def build_parser():
    import argparse

    import pkg_resources
    dist = pkg_resources.get_distribution('libkodi')
    console_scripts = dist.get_entry_map('console_scripts')

    package_parser = argparse.ArgumentParser(description=__doc__)
    subparsers = package_parser.add_subparsers(help='Run one of the kodi subcommands')

    for name, entry_point in console_scripts.iteritems():
        if name == 'kodi':
            continue

        trim_name = '_'.join(name.split('_')[1:])

        main = entry_point.load()
        module = sys.modules[main.__module__]
        parser = subparsers.add_parser(trim_name, help=module.__doc__)
        parser.set_defaults(func=main)

        try:
            module.add_arguments(parser)
        except AttributeError as e:
            pass

    return package_parser

def main(argv=None):
    args = build_parser().parse_args(argv)
    args.func(sys.argv[2:])

#main()
