import requests
from libkodi import kodi

""" KODI Transfer docs """

def add_arguments(parser):
    parser.add_argument('servers', help='Server to ping',
                        metavar='server', nargs='*', default=['localhost'])

def build_parser():
    import argparse
    parser = argparse.ArgumentParser(description=__doc__)
    add_arguments(parser)
    return parser

def main(argv=None):
    args = build_parser().parse_args(argv)

    for server in args.servers:
        kodi_server = kodi.endpoint(server)

        try:
            result = kodi_server.JSONRPC.Ping()
        except kodi.ConnectionError as e:
            result = 'Connection Error'
        except Exception as e:
            result = str(e)

        print '{}: {}'.format(server, result)

if __name__ == '__main__':
    main()
