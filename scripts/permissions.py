import pprint

from libkodi import kodi

""" KODI Update Library docs """

def add_arguments(parser):
    parser.add_argument('servers', help='Server to transfer data between',
                        nargs='+')

def build_parser():
    import argparse
    parser = argparse.ArgumentParser(description=__doc__)
    add_arguments(parser)
    return parser

def main(argv=None):
    args = build_parser().parse_args(argv)

    servers = [kodi.endpoint(server) for server in args.servers]

    for server in servers:
        try:
            pprint.pprint(server.JSONRPC.Permission())
        except kodi.ConnectionError as e:
            print "Connection Error"
            exit(1)
        except Exception as e:
            print e
            exit(1)

if __name__ == '__main__':
    main()
