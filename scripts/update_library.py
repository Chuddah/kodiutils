from libkodi import kodi

""" KODI Update Library docs """

AUDIO, VIDEO, PVR = [2 ** n for n in range(3)]

libraries = {
    'audio': AUDIO,
    'video': VIDEO,
    'pvr':   PVR,
    'all':   AUDIO | VIDEO | PVR,
}

def add_arguments(parser):
    parser.add_argument('servers', help='Server to transfer data between',
                        nargs='+')
    parser.add_argument('--library', help='Library to update', 
                        default='all', choices=libraries.keys())
    parser.add_argument('--clean', help='Clean the library before update.', 
                        default=False, action='store_true')

def build_parser():
    import argparse
    parser = argparse.ArgumentParser(description=__doc__)
    add_arguments(parser)
    return parser

def main(argv=None):
    args = build_parser().parse_args(argv)

    servers = [kodi.endpoint(server) for server in args.servers]
    library_bitmask = libraries[args.library]

    for server in servers:
        try:
            if library_bitmask & AUDIO != 0:
                if args.clean:
                    server.AudioLibrary.Clean()
                server.AudioLibrary.Scan()
            if library_bitmask & VIDEO != 0:
                if args.clean:
                    server.VideoLibrary.Clean()
                server.VideoLibrary.Scan()
            if library_bitmask & PVR != 0:
                server.PVR.Scan()
        except kodi.ConnectionError as e:
            print server, "Connection Error"
            exit(1)
        except Exception as e:
            print server, e
            exit(1)

if __name__ == '__main__':
    main()
