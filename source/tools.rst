Tools
=====

kodi_ping
---------

.. argparse::
    :module: scripts.ping
    :func: build_parser
    :prog: kodi_ping

kodi_playlist
-------------

.. argparse::
    :module: scripts.playlist
    :func: build_parser
    :prog: kodi_playlist

kodi_transfer
-------------

.. argparse::
    :module: scripts.transfer
    :func: build_parser
    :prog: kodi_transfer

kodi_play_youtube
-----------------

.. argparse::
    :module: scripts.play_youtube
    :func: build_parser
    :prog: kodi_play_youtube
