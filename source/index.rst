.. DocTesting documentation master file, created by
   sphinx-quickstart on Thu Nov 17 14:46:33 2016.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to DocTesting's documentation!
======================================

Contents:

.. toctree::
   :maxdepth: 4

   libkodi

.. toctree::
   :maxdepth: 2

   tools

.. automodule:: scripts
    :members:
    :undoc-members:
    :show-inheritance:

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`

